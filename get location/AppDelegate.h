//
//  AppDelegate.h
//  get location
//
//  Created by clicklabs124 on 10/26/15.
//  Copyright (c) 2015 koshal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

