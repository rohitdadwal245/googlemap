//
//  ViewController.h
//  get location
//
//  Created by clicklabs124 on 10/26/15.
//  Copyright (c) 2015 koshal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>
@interface ViewController : UIViewController<CLLocationManagerDelegate>


@end

