//
//  ViewController.m
//  get location
//
//  Created by clicklabs124 on 10/26/15.
//  Copyright (c) 2015 koshal. All rights reserved.
//

#import "ViewController.h"
#import <GoogleMaps/GoogleMaps.h>
@interface ViewController (){
    CLLocationManager *manager;
    UIImageView *imageView;
   
    
}
@property (weak, nonatomic) IBOutlet UILabel *latitude;
@property (weak, nonatomic) IBOutlet UILabel *longitude;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UITextField *txt1;
@property (weak, nonatomic) IBOutlet MKMapView *mapview;
@property (weak, nonatomic) IBOutlet UITextField *txt2;
@end

@implementation ViewController
GMSMapView *mapView;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    imageView = [UIImageView new];
    [imageView setFrame:CGRectMake(10, 10, 10, 10)];
    [imageView setFrame:CGRectMake(0, 0, 500, 400)];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"l.jpg"]];
    [self.view addSubview:imageView];
    
    
    manager = [[CLLocationManager alloc]init];
    manager.delegate = self;
    manager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    [manager startUpdatingLocation];
    [manager requestWhenInUseAuthorization];
    // Do any additional setup after loading the view, typically from a nib.
    
    // Create a GMSCameraPosition that tells the map to display the
    // coordinate -33.86,151.20 at zoom level 6.
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.86
                                                            longitude:151.20
                                                                 zoom:6];
    mapView= [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView.myLocationEnabled = YES;
    self.view = mapView;
    
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(-33.86, 151.20);
    marker.title = @"Sydney";
    marker.snippet = @"Australia";
    marker.map = mapView;
    
}

- (IBAction)GetLocation:(id)sender {
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        _txt1.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        _txt2.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
